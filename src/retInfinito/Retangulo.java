package retInfinito;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Date;

public class Retangulo extends Frame{

	 private int windowHeight;
	 private int largura;
	 private int r;
	 private int l;
	 private int xi;
	 private int yi;
	 private double steps;
	 private AffineTransform initialTransformation;
	 private AffineTransform finalTransformation;
	 private AffineTransform intermediateTransform;
	 private Rectangle2D.Double ret;
	
	 
     //Constructor 
	 /*
	  * Constructor
	  * @param heitght : altura da janela
	  * @param r : raio do circulo
	  * @param l: lado do retangulo
	  * @param largura : largura do da janela
	  * */
	 public  Retangulo(int largura, int height, int r, int l, double steps){	    
		 this.windowHeight = height;
		 this.largura = largura;
		 this.r=r;
		 this.l=l;
		 this.xi=r+2*l; //centro do circulo
		 this.yi=r+2*l; //centro do circulo
		 this.steps=steps;
		 addWindowListener(new MyFinishWindow());
	 }
	 
	 
	 public void paint(Graphics g){
	
		 Graphics2D g2d = (Graphics2D) g;	

		 //deixa o plano de coordenadas na forma "real"	
		 AffineTransform yUp = new AffineTransform();
		 yUp.setToScale(1,-1);
		 AffineTransform translate = new AffineTransform();
		 translate.setToTranslation(0,windowHeight);
		 yUp.preConcatenate(translate);
         g2d.transform(yUp);
		 
		 //muda a grossura da linha
		 BasicStroke bs = new BasicStroke(3.0f);
		 g2d.setStroke(bs);

		 //cria o retangulo 
		 ret = new Rectangle2D.Double(0,0,2*l,l);
		 
		 ArrayList<Coordenada> trajetoria; 
		 trajetoria = calcTrajetoria(this.r);  
		 
		 double xmed[] = new double[] {-l,-l/4.0,-l/2.0,-l/4.0,l/2.0};
	     double ymed[] = new double[] {-l/2.0,-l/4.0,-l/2.0,-l/4.0,-l/2.0};
	   
		 //transformacao inicial no angulo PI
	     double initialMatrix [] = new double[6];
	     initialTransformation = new AffineTransform();	 
		 initialTransformation.setToTranslation(xi+trajetoria.get(0).getCoordenadaX()+xmed[0],yi+trajetoria.get(0).getCoordenadaY()+ymed[0]);
		 initialTransformation.getMatrix(initialMatrix); 
		 
		 double finalMatrix[] = new double[6];
			 	         
	     int octante = 0; //na verdade é o 5 octante
	     double rotate[] = new double[] {0, Math.PI/4 , Math.PI/2 , Math.PI/2};
	     double scaleX[] = new double[] {0.25,0.5,0.25,0.5};
	     double scaleY[] = new double[] {0.5,1,0.5,2};
	     	    	     
	     for(int j = 0 ; j<4 ; j++ ){
	    	 
	    	 octante=octante-1+trajetoria.size()/4;
	
	    	 finalTransformation = new AffineTransform();
			 finalTransformation.setToTranslation(xi+trajetoria.get(octante).getCoordenadaX()+xmed[j],yi+trajetoria.get(octante).getCoordenadaY()+ymed[j]);
			 finalTransformation.rotate(rotate[j]);	
		     finalTransformation.scale(scaleX[j],scaleY[j] );
			 finalTransformation.getMatrix(finalMatrix);
			 
			 for (double i = 0 ; i <= steps; i++){       	 
	          	//transformacao intermediaria entre os angulos
	        	intermediateTransform = new AffineTransform(
	                     convexCombination(initialMatrix,finalMatrix,i/steps));
	        	
	        	sustain(80);
	           	clearWindow(g2d,largura, windowHeight);
	         	g2d.draw(intermediateTransform.createTransformedShape(ret));
	       	     
	          }//fim for
	        
	       initialTransformation = new AffineTransform(finalTransformation);
	       initialTransformation.getMatrix(initialMatrix);
	     
	     }//fim for

	     //segundo circulo
	     
	     //altera o (x,y) central
	     this.xi = xi+2*r;
	
		 initialTransformation = new AffineTransform(finalTransformation);
		 initialTransformation.rotate(0);
		 initialTransformation.getMatrix(initialMatrix);	 
		  
		 octante=0;
	     rotate[0]= Math.PI/2;
	    
		 for(int j = 0 ; j < 4 ; j++ ){
	    	 
	    	 octante = octante-1+trajetoria.size()/4;
	    	 
	    	 finalTransformation = new AffineTransform();
			 finalTransformation.setToTranslation(xi+trajetoria.get(octante).getCoordenadaX()+xmed[j],yi+trajetoria.get(octante).getCoordenadaY()+ymed[j]);
			 finalTransformation.rotate(rotate[j]);	
		     finalTransformation.scale(scaleX[j],scaleY[j] );
			 finalTransformation.getMatrix(finalMatrix);
		    
			 for (double i = 0 ; i <= steps; i++){       	 
	          	//transformacao intermediaria entre os angulos
	        	intermediateTransform = new AffineTransform(
	                     convexCombination(initialMatrix,finalMatrix,i/(steps)));
	        	
	        	sustain(80);
	           	clearWindow(g2d, largura, windowHeight);
	        	g2d.draw(intermediateTransform.createTransformedShape(ret));
	       	     
	          }//fim for
	        
		   initialTransformation = new AffineTransform(finalTransformation);
	       initialTransformation.getMatrix(initialMatrix);
	     }//fim for
		 
 }

	 	public static ArrayList<Coordenada> calcTrajetoria(double r){
			
	 		ArrayList<Coordenada> octante1 = new ArrayList<Coordenada>();
	 		ArrayList<Coordenada> listaCoord = new ArrayList<Coordenada>();
			Coordenada aux;
		     
			 //cria trajetoria do circulo
			 double p = 1-r;
			 double x=0.0,y=r; //considera como se o centro fosse (0,R)
			
			 while(x<y){
				 aux= new Coordenada();
				 x++;
				 if(p<0.0){
					 p=p+2*x+1;
				 }else{
					 y--;
					 p=p+2*x+1-2*y;
				 }
				 
				 aux.setCoordenada(y,x);
				 octante1.add(aux);
		 
			 }
			 
			 //lista de coordenadas da parte de baixo do circulo calculadas apartir do octante 1
			 // essa lista eh pra caso precise desenhar o meio infinito
			
			 //octante 5
			for (int i = 0; i< octante1.size() ; i++){
				 aux = new Coordenada();
				 aux.setCoordenada(-octante1.get(i).getCoordenadaX(), -octante1.get(i).getCoordenadaY());
				 listaCoord.add(aux);				 
				 }
			
			//octante 6
			for (int i = octante1.size() -1 ; i >= 0  ; i--){
				 aux = new Coordenada();
				 aux.setCoordenada(-octante1.get(i).getCoordenadaY(), -octante1.get(i).getCoordenadaX());
				 listaCoord.add(aux);				 
				 }
			
			//octante 7
			for (int i = 0; i< octante1.size() ; i++){
				 aux = new Coordenada();
				 aux.setCoordenada(octante1.get(i).getCoordenadaY(), -octante1.get(i).getCoordenadaX());
				 listaCoord.add(aux);				 
				 }
			
			//octante 8
			for (int i = octante1.size() -1 ; i >= 0  ; i--){
				 aux = new Coordenada();
				 aux.setCoordenada(octante1.get(i).getCoordenadaX(), -octante1.get(i).getCoordenadaY());
				 listaCoord.add(aux);				 
				 }
	 		
			return listaCoord;
			
	 	}
	 
	 
		public static void sustain(long t)
		{
		  long finish = (new Date()).getTime() + t;
		  while( (new Date()).getTime() < finish ){}
		}
		
		public static void clearWindow(Graphics2D g, int largura, int altura)
		{
		  g.setPaint(Color.white);
		  g.fill(new Rectangle(0,0,largura,altura));
		  g.setPaint(Color.black);
		}
		
		
		public static double[] convexCombination(double[] a, double[] b, double alpha)
		{
		  double[] result = new double[a.length];
		
		  for (int i=0; i<result.length; i++)
		  {
		    result[i] = (1-alpha)*a[i] + alpha*b[i];
		  }
		
		  return(result);
		}
		
		

}
