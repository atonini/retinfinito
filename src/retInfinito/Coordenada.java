package retInfinito;

public class Coordenada {
	private double x;
	private double y;
	
	public void setCoordenada(double x, double y){
		this.x=x;
		this.y=y;
	}
	
	public double getCoordenadaX(){
		return this.x;
		
	}
	
	public double getCoordenadaY(){
		return this.y;
		
	}
	
}
