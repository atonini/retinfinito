package retInfinito;

import java.util.Scanner;

/*
 * Autor : Aline Tonini
 * Data 11/04/2014
 * 
 * */


public class Main {
	
	  public static void main(String[] argv)
	  {
	    int r=0, l=0;
	    double steps=0;
	    
	    Scanner entrada = new Scanner(System.in);  
	    
	    while (r<=0){
	    	System.out.println("Digite o valor de R em inteiros :");
	    	r = entrada.nextInt();
	    }
	    
	    while(l<=0){
	    	System.out.println("Digite o valor de L em inteiros :");
	    	l = entrada.nextInt();
	    }
	    
	    while(steps<=0){
	    	System.out.println("Digite o valor o numero de steps em double :");
	    	steps = entrada.nextDouble();
	    }
	    
	    entrada.close();
	    int altura =r*2+3*l;
	    int largura = r*4+3*l;
	    
	    Retangulo ret = new Retangulo(largura, altura, r, l, steps);
	    
	    ret.setTitle("Retangulo no Infinito");
	    ret.setSize(largura,altura);
	    ret.setVisible(true);
	  }


}
