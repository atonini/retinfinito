#####################################################################################################################
#                                  retInfinito - Trabalho 1 de Computação Gráfica 
#####################################################################################################################
#
#   1. Introduçao
#   2. Implementação
#	  2.1. Classes
#   3. Resultados e Conclusão
#   
#   @autor Aline Rodrigues Tonini
#   @version 1.0
#
#####################################################################################################################
#   1. Introduçao
#####################################################################################################################
#
#       O projeto retInfinito se propõem a realizar o trabalho de Computação Gráfica, proposto pelo Prof. Marilton Aguiar.
#    	Com o algoritmo do ponto médio para desenho de círculos, calcular uma trajetória da metade inferior do símbolo
#		de infinito. Esta lista dos pontos calculados pelo algoritmo será dividida apropriadamente em 8 segmentos,
#		correspondendo aos pontos em que o objeto terminará de mudar sua forma.
#
#		É implementado a combinação convexa para animar a movimentação linear com S steps de um objeto geométrico 
#		pelos nove pontos da trajetória. 
#
#		No ponto inicial, o retângulo terá tamanho 2L×L. No seguinte, o retângulo terá tamanho L/2 × L/2. No posterior,
#		o retângulo terá tamanho L×L e será rotacionado 45 graus. Depois, retornará ao estado anterior e, por conseguinte, ao estágio inicial,
#		completando a primeira metade da trajetória. O mesmo ocorrerá para a segunda metade da trajetória.
#
#		O tamanho da janela deve ser calculado de modo que toda a animação possa ser observada, sem cortes. 
#		
#		Os valores de L, R e S são parâmetros do programa (devem ser passados por linha de comando ou pela interface).
#	        
#####################################################################################################################
#   2. Implementação
#####################################################################################################################
#
#       Na implementação do Projeto utilizou -se Java2D e os paradigmas da  orientação a objeto. 
#		O trabalho fou desenvolvido utilizando a IDE Eclise e o versionador Bitbucket para manter
#		o código organizado.
#
#   2.1 Classes do Projeto
#        Classe Main: Classe principal onde os parametros R,L e S são passados para o programa,
#					e também onde é definido o tamanho da janela.
#		 Classe Retangulo : Classe que extende a classe Frame e implementa o método paint()
#							No método paint() é definido e desenhado o retângulo e implementado 
#							as interpolações e a trajetória.
#		 					No método calcTrajetoria() é calculado os pontos do primeiro quadrante do 
#		 					circulo com a formula do ponto médio. Após esse pontos serem calculdos eles
#		 					são espelhados para os outros quadrantes e armazenados em uma lista
#		 					que pode ser usada para desenhar a parte inferior do circulo.
#		 					O método sustain() é utilizado para deixar o for mais lento e permitir a visualização
#		 					da imagem.
#		 					O método clearWindow() limpa a tela de visualização.
#		 					O método convexCombination() realiza a interpolação.
#		Classe Coordenada : Essa classe é usada para implementar as coordenadas de cada ponto.
#		Classe MyFinishWindow: Implementa um listener para fechar a janela da animação com
#							com um clique.
#
#####################################################################################################################
# 	3. Resultados e Conclusão
#####################################################################################################################
#
#       O resultado da execução é a animação de um retângulo que muda sua escala e rotação enquanto caminha
#		na parte de baixo do símbolo do infinito.
#		Com este trabalho foi compreendido como funciona as animações em Java 2D, como fazer transformações e 
#		combinação de transformações, a combinação convexa além do algoritmo do ponto médio para círculos.
#		Também foi importante para relembrar conceitos de geometria.
#
#####################################################################################################################